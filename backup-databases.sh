# Script to dump MySQL databases
# Copyright (C) 2012, Matt Lee <mattl@cnuk.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash

DRUPAL="/var/sites/"
BACKUP="/root" # no trailing slash

echo "Backing up Drupal databases for sites in "$DRUPAL

echo " "

for i in "$DRUPAL"*/drupal/sites/default/custom.settings.php
do
    DRUPALDB=`grep 'drup_db_base' $i | awk '{print $3'} | sed -e "s/;//g" | sed -e "s/'//g" | head -n 1`;
    CIVIDB=`grep 'civi_db_base' $i | awk '{print $3'} | sed -e "s/;//g" | sed -e "s/'//g" | head -n 1`;
    DBUSER=`grep 'drup_db_user' $i | awk '{print $3'} | sed -e "s/;//g" | sed -e "s/'//g" | head -n 1`;
    DBPASS=`grep 'drup_db_pass' $i | awk '{print $3'} | sed -e "s/;//g" | sed -e "s/'//g" | head -n 1`;

    echo -n "Backing up"$DRUPALDB"..."

    BACKUPFILENAME="$BACKUP"/`date -I`-$DRUPALDB.sql

    mysqldump -u $DBUSER -p$DBPASS $DRUPALDB > $BACKUPFILENAME

    echo " done! ("$BACKUPFILENAME")"

    echo -n "Backing up"$CIVIDB"..."

    BACKUPFILENAME="$BACKUP"/`date -I`-$CIVIDB.sql

    mysqldump -u $DBUSER -p$DBPASS $CIVIDB > $BACKUPFILENAME

    echo " done! ("$BACKUPFILENAME")"

done
